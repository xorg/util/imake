#
# Copyright (c) 2025, Oracle and/or its affiliates.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
#

project(
  'imake', 'c',
  version: '1.0.10',
  meson_version: '>=1.1.0',
  license : 'MIT-open-group AND MIT',
  license_files : 'COPYING'
)
imake_conf = configuration_data()
scripts_conf = configuration_data()
scripts_conf.set(
  'PACKAGE_STRING',
  meson.project_name() + ' ' + meson.project_version()
)
cc = meson.get_compiler('c')

# Find cpp program for use in pre-processing Imakefiles
# (replaces XORG_PROG_RAWCPP from xorg-macros.m4 for autoconf, but drops
#  the checks for specific flags, since imake didn't use those)
# To match the autoconf behavior, search $PATH first, then specific locations
cpp = find_program('cpp', required: false)
if not cpp.found()
  cpp = find_program(
    'cpp', required: true,
    dirs: ['/bin', '/usr/bin', '/usr/lib', '/usr/libexec',
           '/usr/ccs/lib', '/usr/ccs/lbin', '/lib']
  )
endif
rawcpp = cpp.full_path()
imake_conf.set_quoted('CPP_PROGRAM', rawcpp)
scripts_conf.set('RAWCPP', rawcpp)

# Checks for library functions.
imake_conf.set('HAVE_MKSTEMP', cc.has_function('mkstemp') ? '1' : false)

xconfdir = join_paths(
  get_option('prefix'),
  get_option('libdir'),
  get_option('config-dir')
)
scripts_conf.set('XCONFDIR', xconfdir)

scripts_conf.set('PREPROCESSCMD_MKDEPEND', get_option('script-preproc-cmd'))
scripts_conf.set('ARCMD', get_option('create-lib-cmd'))
scripts_conf.set('RANLIB', get_option('clean-lib-cmd'))

xproto_dep = dependency('xproto', required: true)


if get_option('revpath').allowed()
  executable('revpath', 'revpath.c', install: true)
  revpath_man = configure_file(
    configuration: scripts_conf,
    output: 'revpath.1',
    input: 'revpath.1.in'
  )
  install_man(revpath_man)
endif

configure_file(configuration: imake_conf, output: 'config.h')
executable(
  'imake', 'imake.c',
  dependencies: xproto_dep,
  install: true
  )
imake_man = configure_file(
  configuration: scripts_conf,
  output: 'imake.1',
  input: 'imake.1.in'
)
install_man(imake_man)


if get_option('makeg').allowed()
  install_data(
    'makeg',
    install_mode: 'rwxr-xr-x',
    install_dir: join_paths(get_option('prefix'), get_option('bindir'))
  )
  makeg_man = configure_file(
    configuration: scripts_conf,
    output: 'makeg.1',
    input: 'makeg.1.in'
  )
  install_man(makeg_man)
endif


if get_option('xmkmf').allowed()
  xmkmf = configure_file(
    configuration: scripts_conf,
    output: 'xmkmf',
    input: 'xmkmf.in',
    install: true,
    install_mode: 'rwxr-xr-x',
    install_dir: join_paths(get_option('prefix'), get_option('bindir'))
  )
  xmkmf_man = configure_file(
    configuration: scripts_conf,
    output: 'xmkmf.1',
    input: 'xmkmf.1.in'
  )
  install_man(xmkmf_man)
endif


if get_option('ccmakedep').allowed()
  ccmakedep = configure_file(
    configuration: scripts_conf,
    output: 'ccmakedep',
    input: 'ccmakedep.in',
    install: true,
    install_mode: 'rwxr-xr-x',
    install_dir: join_paths(get_option('prefix'), get_option('bindir'))
  )
  ccmakedep_man = configure_file(
    configuration: scripts_conf,
    output: 'ccmakedep.1',
    input: 'ccmakedep.1.in'
  )
  install_man(ccmakedep_man)
endif


if get_option('mergelib').allowed()
  mergelib = configure_file(
    configuration: scripts_conf,
    output: 'mergelib',
    input: 'mergelib.in',
    install: true,
    install_mode: 'rwxr-xr-x',
    install_dir: join_paths(get_option('prefix'), get_option('bindir'))
  )
  mergelib_man = configure_file(
    configuration: scripts_conf,
    output: 'mergelib.1',
    input: 'mergelib.1.in'
  )
  install_man(mergelib_man)
endif


if get_option('mkdirhier').allowed()
  install_data(
    'mkdirhier',
    install_mode: 'rwxr-xr-x',
    install_dir: join_paths(get_option('prefix'), get_option('bindir'))
  )
  mkdirhier_man = configure_file(
    configuration: scripts_conf,
    output: 'mkdirhier.1',
    input: 'mkdirhier.1.in'
  )
  install_man(mkdirhier_man)
endif


if get_option('cleanlinks').allowed()
  install_data(
    'cleanlinks',
    install_mode: 'rwxr-xr-x',
    install_dir: join_paths(get_option('prefix'), get_option('bindir'))
  )
  cleanlinks_man = configure_file(
    configuration: scripts_conf,
    output: 'cleanlinks.1',
    input: 'cleanlinks.1.in'
  )
  install_man(cleanlinks_man)
endif


if get_option('mkhtmlindex').allowed()
  perl = find_program('perl', required: false)
  if perl.found()
    mkhtmlindex = 'mkhtmlindex.pl'
  else
    mkhtmlindex = 'mkhtmlindex.sh'
  endif

  install_data(
    mkhtmlindex,
    rename: 'mkhtmlindex',
    install_mode: 'rwxr-xr-x',
    install_dir: join_paths(get_option('prefix'), get_option('bindir'))
  )
  mkhtmlindex_man = configure_file(
    configuration: scripts_conf,
    output: 'mkhtmlindex.1',
    input: 'mkhtmlindex.1.in'
  )
  install_man(mkhtmlindex_man)
endif
